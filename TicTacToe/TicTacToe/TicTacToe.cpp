#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> 

const char* CSI = "\x1b[";
const char* SAVE_CURSOR_POS = "\x1b[s";
const char* RESTORE_CURSOR_POS = "\x1b[u";

char realboard[3][3] = { '-', '-', '-', '-', '-', '-', '-', '-', '-' };
int choice;
bool finished = false;
//bool again = false;
bool legal = false;
int nonlegal;
unsigned int turn = 1;

void printPositions()
{
    int board[3][3] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    std::cout << "Positions: " << std::endl << std::endl;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            std::cout << board[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << SAVE_CURSOR_POS;
}

void printGameBoard()
{
    system("cls");
    
    // print positions
    printPositions();

    // print board
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            std::cout << realboard[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return;
}

void clearInputBuffer()
{
    std::cin.clear();
    std::cin.ignore(std::cin.rdbuf()->in_avail());
}

void player1Turn() 
{   
    turn++;
    clearInputBuffer();

    std::cout << "Player 1, select a position... " << std::endl; 
    std::cin >> choice;
    
    if (!std::cin.good()) {      
        std::cout << "Invalid input... " << std::endl;

        turn--;
        return;
    }

    if (choice == 1) {
        if (realboard[0][0] != 'x') {
            realboard[0][0] = 'o';
        }
        else if (realboard[0][0] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 2) {
        if (realboard[0][1] != 'x') {
            realboard[0][1] = 'o';
        }
        else if (realboard[0][1] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 3) {
        if (realboard[0][2] != 'x') {
            realboard[0][2] = 'o';
        }
        else if (realboard[0][2] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 4) {
        if (realboard[1][0] != 'x') {
            realboard[1][0] = 'o';
        }
        else if (realboard[0][1] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 5) {
        if (realboard[1][1] != 'x') {
            realboard[1][1] = 'o';
        }
        else if (realboard[1][1] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 6) {
        if (realboard[1][2] != 'x') {
            realboard[1][2] = 'o';
        }
        else if (realboard[1][2] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 7) {
        if (realboard[2][0] != 'x') {
            realboard[2][0] = 'o';
        }
        else if (realboard[2][0] == 'x') {
            turn--;
            return;
        }
    }
    else if (choice == 8) {
        if (realboard[2][1] != 'x') {
            realboard[2][1] = 'o';
        }
        else if (realboard[2][1] == 'x') {          
            turn--; 
            return;
        }
    }
    else if (choice == 9) {
        if (realboard[2][2] != 'x') {
            realboard[2][2] = 'o';
        }
        else if (realboard[2][2] == 'x') {
            turn--;
            return;
        }
    }
    else {
        turn--;
        return;
    }
    
    return;
}

void player2Turn() 
{
    turn++;
    std::cout << "Player 2, select a position... " << std::endl;
    clearInputBuffer();
    std::cin >> choice;

    if (!std::cin.good()) {
        std::cout << "Invalid input... " << std::endl;

        turn--;
        return;
    }

    if (choice == 1) {
        if (realboard[0][0] != 'o') {
            realboard[0][0] = 'x';
        }
        else if (realboard[0][0] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 2) {
        if (realboard[0][1] != 'o') {
            realboard[0][1] = 'x';
        }
        else if (realboard[0][1] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 3) {
        if (realboard[0][2] != 'o') {
            realboard[0][2] = 'x';
        }
        else if (realboard[0][2] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 4) {
        if (realboard[1][0] != 'o') {
            realboard[1][0] = 'x';
        }
        else if (realboard[0][1] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 5) {
        if (realboard[1][1] != 'o') {
            realboard[1][1] = 'x';
        }
        else if (realboard[1][1] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 6) {
        if (realboard[1][2] != 'o') {
            realboard[1][2] = 'x';
        }
        else if (realboard[1][2] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 7) {
        if (realboard[2][0] != 'o') {
            realboard[2][0] = 'x';
        }
        else if (realboard[2][0] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 8) {
        if (realboard[2][1] != 'o') {
            realboard[2][1] = 'x';
        }
        else if (realboard[2][1] == 'o') {
            turn--; 
            return;
        }
    }
    else if (choice == 9) {
        if (realboard[2][2] != 'o') {
            realboard[2][2] = 'x';
        }
        else if (realboard[2][2] == 'o') {
            turn--; 
            return;
        }
    }
    else {
        turn--;
        return;
    }

    return;
}

void WinCheck() 
{
    if (realboard[0][0] == 'o' && realboard[0][1] == 'o' && realboard[0][2] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[1][0] == 'o' && realboard[1][1] == 'o' && realboard[1][2] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[2][0] == 'o' && realboard[2][1] == 'o' && realboard[2][2] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[0][0] == 'o' && realboard[1][0] == 'o' && realboard[2][0] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[0][1] == 'o' && realboard[1][1] == 'o' && realboard[2][1] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[0][2] == 'o' && realboard[1][2] == 'o' && realboard[2][2] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[0][0] == 'o' && realboard[1][1] == 'o' && realboard[2][2] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[0][2] == 'o' && realboard[1][1] == 'o' && realboard[2][0] == 'o') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 1 Wins!" << std::endl;
    }
    else if (realboard[0][0] == 'x' && realboard[0][1] == 'x' && realboard[0][2] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[1][0] == 'x' && realboard[1][1] == 'x' && realboard[1][2] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[2][0] == 'x' && realboard[2][1] == 'x' && realboard[2][2] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[0][0] == 'x' && realboard[1][0] == 'x' && realboard[2][0] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[0][1] == 'x' && realboard[1][1] == 'x' && realboard[2][1] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[0][2] == 'x' && realboard[1][2] == 'x' && realboard[2][2] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[0][0] == 'x' && realboard[1][1] == 'x' && realboard[2][2] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (realboard[0][2] == 'x' && realboard[1][1] == 'x' && realboard[2][0] == 'x') {
        finished = true;
        std::cout << std::endl;
        std::cout << "Player 2 Wins!" << std::endl;
    }
    else if (turn == 10) {
        finished = true;
        std::cout << std::endl;
        std::cout << "Tie Game!" << std::endl;
    }
    else {
        return;
    }

    return;
}

void main()
{       
    printGameBoard();

    do 
    {
        WinCheck();
        if (finished == false) 
        {
            std::cout << RESTORE_CURSOR_POS;
            std::cout << CSI << "4M";
            printGameBoard();

            if (turn == 1 || turn == 3 || turn == 5 || turn == 7 || turn == 9) {
                player1Turn();
            }
            else {
                player2Turn();
            }
        }

    } while (finished == false);
    
    std::cout << std::endl;

    Sleep(2000);

    return;
}